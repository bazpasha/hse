#include <iostream>
#include <typeinfo>
#include <vector>

class Heap {
    struct vertex {
        size_t key;
        int value;

        bool operator>(vertex other) {
            return value > other.value;
        }
    };

    std::vector<vertex> heap;
    std::vector<size_t> place;
    size_t size_;

    size_t parent(size_t index) {
        if (index == 0)
            return 0;
        else if (index % 2 == 0)
            return index / 2 - 1;
        else
            return index / 2;
    }

    void print() {
        std::cout << "***\n";
        for (size_t i = 0; i < size_; ++i)
            std::cout << heap[i].key << " " << heap[i].value << "\n";
        std::cout << "***\n";
    }

    void siftUp(size_t index) {
        while (index != 0 && heap[parent(index)] > heap[index]) {
            std::swap(heap[parent(index)], heap[index]);
            std::swap(place[parent(index)], place[index]);
            index = parent(index);
        }
    }

    void siftDown(size_t index) {
         while (2 * index + 1 < size_ && heap[index] > heap[2 * index + 1] || heap[index] > heap[2 * index + 2]) {
             if (2 * index + 2 < size_ && heap[2 * index + 1] > heap[2 * index + 2]) {
                 std::swap(heap[index], heap[2 * index + 2]);
                 std::swap(place[index], place[2 * index + 2]);
                 index = 2 * index + 2;
             } else {
                 std::swap(heap[index], heap[2 * index + 1]);
                 std::swap(place[index], place[2 * index + 1]);
                 index = 2 * index + 1;
             }
         }
    }

 public:
    Heap(size_t n, int starting = 0) {
        heap.resize(n);
        place.resize(n);
        size_ = n;
        for (size_t i = 0; i < size_; ++i) {
            heap[i] = {i, starting};
            place[i] = i;
        }
    }

    template<typename iterator>
    Heap(iterator begin, iterator end) {
        heap.resize(end - begin);
        place.resize(end - begin);
        size_ = 0;
        auto it = begin;
        for (size_t i = 0; i < end - begin; ++i) {
            heap[i] = {i, *it};
            place[i] = i;
            ++size_;
            siftUp(i);
            ++it;
        }
    }

    Heap(std::initializer_list<int> elements) {
        heap.resize(elements.size());
        place.resize(elements.size());
        size_ = 0;
        auto it = elements.begin();
        for (size_t i = 0; i < elements.size(); ++i) {
            heap[i] = {i, *it};
            place[i] = i;
            ++size_;
            siftUp(i);
            ++it;
        }
    }

    size_t size() const {
        return size_;
    }

    bool empty() const {
        return size_ == 0;
    }

    void decrease_key(size_t key, int value) {
        size_t index = place[key];
        heap[index] = {key, value};
        if (heap[index] > heap[parent(index)])
            siftDown(index);
        else
            siftUp(index);
    }

    size_t top() const {
        return heap[0].key;
    }

    void pop() {
        heap[0] = heap[size_ - 1];
        --size_;
        siftDown(0);
    }

};

int calculate_min_cost() {

    struct edge {
        size_t to;
        int cost;
    };

    const int INF = 1000 * 1000 * 1000;
    int n, m, k;
    std::cin >> n >> m;
    Heap dijkstra(n, INF);
    std::vector<std::vector<edge>> siblings(n);

    size_t from, to;
    for (size_t i = 0; i < m; ++i) {
        std::cin >> from >> to;
        --from, --to;
        siblings[from].push_back({to, 0});
        siblings[to].push_back({from, 0});
    }

    std::cin >> k;
    int cost;
    for (size_t i = 0; i < k; ++i) {
        std::cin >> from >> to >> cost;
        --from, --to;
        siblings[from].push_back({to, cost});
        siblings[to].push_back({from, cost});
    }

    size_t x, y;
    std::cin >> x >> y;
    --x, --y;

    dijkstra.decrease_key(x, 0);
    std::vector<int> roads(n, INF);
    roads[x] = 0;
    std::vector<bool> marked(n, false);
    size_t v;
    while (dijkstra.top() != y && roads[dijkstra.top()] != INF) {
        v = dijkstra.top();
        dijkstra.pop();
        marked[v] = true;
        for (edge e : siblings[v]) {
            if (!marked[e.to] && e.cost + roads[v] < roads[e.to]) {
                roads[e.to] = e.cost + roads[v];
                dijkstra.decrease_key(e.to, e.cost + roads[v]);
            }
        }
    }

    if (roads[y] == INF)
        return -1;
    else
        return roads[y];
}

int main() {
    std::vector<int> v = {5, 7, 19, 3, 66, -7, 15};
    Heap h(v.begin(), v.end());
    std::cout << h.top();
    return 0;
}